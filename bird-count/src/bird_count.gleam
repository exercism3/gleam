import gleam/list
import gleam/int

pub fn today(days: List(Int)) -> Int {
  case days {
    [] -> 0
    [today, ..] -> today
  }
}

pub fn increment_day_count(days: List(Int)) -> List(Int) {
  case days {
    [] -> [1]
    [today, ..tail] -> [today + 1, ..tail]
  }
}

pub fn has_day_without_birds(days: List(Int)) -> Bool {
  case days {
    [] -> False
    [0, .._] -> True
    [_head, ..tail] -> has_day_without_birds(tail)
  }
}

pub fn total(days: List(Int)) -> Int {
  days
  |> list.fold(0, int.add)
}

pub fn busy_days(days: List(Int)) -> Int {
  days
  |> list.filter(fn(x) { x > 4 })
  |> list.length
}
