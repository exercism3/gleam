// TODO: please define the Pizza custom type
pub type Pizza {
  Margherita
  Caprese
  Formaggio
  ExtraSauce(Pizza)
  ExtraToppings(Pizza)
}

pub fn pizza_price(pizza: Pizza) -> Int {
  do_pizza_price(pizza, 0)
}

fn do_pizza_price(pizza: Pizza, acc: Int) -> Int {
  case pizza {
    Margherita -> acc + 7
    Caprese -> acc + 9
    Formaggio -> acc + 10
    ExtraSauce(pizza) -> acc + 1 + pizza_price(pizza)
    ExtraToppings(pizza) -> acc + 2 + pizza_price(pizza)
  }
}

pub fn order_price(order: List(Pizza)) -> Int {
  case order {
    [] -> 0
    [pizza] -> 3 + pizza_price(pizza)
    [_, _] -> calc_order_price(order, 2)
    _ -> calc_order_price(order, 0)
  }
}

fn calc_order_price(order: List(Pizza), acc: Int) -> Int {
  case order {
    [] -> acc
    [pizza, ..rest] -> calc_order_price(rest, acc + pizza_price(pizza))
  }
}
